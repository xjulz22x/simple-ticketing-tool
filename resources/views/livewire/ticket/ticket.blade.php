<div>
    @include('livewire.ticket.ticket-modal')
    @include('livewire.ticket.print')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 col-12 align-self-center">
                <h3 class="text-themecolor mb-0">Tickets</h3>
                
            </div>
            <div class="col-md-7 col-12 align-self-center d-none d-md-block">
                <div class="d-flex mt-2 justify-content-end">
                    <div class="d-flex mr-3 ml-2">
                        <div class="chart-text mr-2">
                            <button type="button" class="btn btn-success" data-toggle="modal"
                            data-target="#ticket-modal"><i class="fas fa-check"></i>
                                Create a ticket</button>
                        </div>
                        
                    </div>
                 
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- Row -->
            {{-- <div class="card">
                <div class="card-body border">
                    <div class="form-group">
                        <label>Upload Ticket Image</label>
                        <input type="file" class="form-control @error('image') is-invalid @enderror" id="exampleInputFile"
                            aria-describedby="fileHelp" wire:model="image">
                            @error('image')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Number of Tickets</label>
                        <input type="number" class="form-control @error('ticketNumber') is-invalid @enderror" id="exampleInputPassword1"
                            placeholder="Password" wire:model="ticketNumber" min="1">
                            @error('ticketNumber')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                    </div>
                <div class="row d-flex justify-content-end">
                    <button type="button" class="btn btn-danger mr-2"
                    data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success mr-2" wire:click="save">Proceed</button>
                </div>
                    
                </div>
            </div> --}}
           
           
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2024 Bambam Berry
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
</div>
<script>
    window.addEventListener('closeTicketModal', event => {
        $('#ticket-modal').modal('hide')

    })
</script>

<script>
    window.addEventListener('printTicketModal', event => {
        $('#ticket-modal').modal('hide')
        $('#print-modal').modal('show')

    })
</script>
<script src="{{asset('dist/js/pages/samplepages/jquery.PrintArea.js')}}"></script>
