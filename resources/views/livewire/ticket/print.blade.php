{{-- <style>
    .rotate-paragraph {
        -ms-writing-mode: tb-rl;
        -webkit-writing-mode: vertical-rl;
        writing-mode: vertical-rl;
        transform: rotate(180deg);
        white-space: nowrap;
        
    }
</style> --}}
<div id="print-modal" class="modal fade" data-backdrop="static"  tabindex="-1" role="dialog"
    aria-labelledby="fullWidthModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-scrollable modal-full-width">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="fullWidthModalLabel">Print</h4>
                <button type="button" class="close" data-dismiss="modal"
                    aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="card card-body printableArea">
                    <div class="row">
                        @for($i = $start; $i <= $end; $i++)
                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-body border p-0">
                                            <div class="row">
                                                <div class="col-md-2 d-flex">
                                                    <p style="font-size: 12px; -ms-writing-mode: tb-rl;
                                                    -webkit-writing-mode: vertical-rl;
                                                    writing-mode: vertical-rl;
                                                    transform: rotate(180deg);
                                                    white-space: nowrap;"><strong>Ticket No: {{$i}}</strong></p>
                                                </div>
                                                <div class="col-md-10">
                                                    <img class="w-100" src="{{ asset('/storage/'.$image) }}">
                                                </div>
                                                
                                            </div>
                                            <div class="row mt-1">
                                                <div class="col-md-12 d-flex justify-content-end align-items-center">
                                                    <p class="pr-3"><strong>Ticket No: {{$i}}</strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                     
                                </div>
                        @endfor
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="submit"> Discard</button>
                <button id="print" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    $(function() {
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });
    </script>