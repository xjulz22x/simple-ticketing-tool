<div wire:ignore.self id="ticket-modal" data-backdrop="static" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="success-header-modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-colored-header bg-success">
                <h4 class="modal-title text-white" id="success-header-modalLabel">Create Ticket
                </h4>
                <button type="button" class="close" data-dismiss="modal"
                    aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Upload Ticket Image</label>
                    <input type="file" class="form-control @error('image') is-invalid @enderror" id="exampleInputFile"
                        aria-describedby="fileHelp" wire:model="image">
                        @error('image')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Start</label>
                    <input type="number" class="form-control @error('start') is-invalid @enderror" id="exampleInputPassword1"
                        placeholder="Start Number" wire:model="start" min="1">
                        @error('start')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">End</label>
                    <input type="number" class="form-control @error('end') is-invalid @enderror" id="exampleInputPassword1"
                        placeholder="End Number" wire:model="end" min="1">
                        @error('end')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light"
                    data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" wire:click="save">Proceed</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->