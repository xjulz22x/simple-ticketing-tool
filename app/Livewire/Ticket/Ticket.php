<?php

namespace App\Livewire\Ticket;

use Livewire\Component;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\View;
use Livewire\WithFileUploads;
use App\Models\Tickets;

class Ticket extends Component
{
    use WithFileUploads;

    public $image;
    public $start;
    public $end;
    public $numbers;

    protected $rules = 
    [
        'image' => 'required|image|max:10000',
        'start' => 'required',
        'end' => 'required',
    ];

    function resetInputFields()
    {
        $this->image = '';
        $this->ticketNumber = '';

    }

    public function closeModal()
    {
        $this->resetInputFields();
        $this->dispatch('closeTicketModal');
    }
    public function save()
    {
        $this->validate();
        $this->dispatch('printTicketModal');
        $path = $this->image->storeAs('temp', $this->image->getClientOriginalName(), 'public');
        $this->image = $path;
        return $path;

        
        
    }

    public function generatePdf()
    {
        $data = $this->ticketNumber;
        $pdf = Pdf::loadView('livewire.ticket.print', $data);
    }

    public function render()
    {
        return view('livewire.ticket.ticket')
        ->layout('layouts.app');
    }
}
